#!/usr/bin/env bash
# Convert various notebook-based import tests to Python (to additionally test that they work there)

set -e

for style in abs; do
  for type in py nb; do
    name="${style}_${type}"
    nb="nb_${name}.ipynb"
    py="py_${name}.py"
    jupyter nbconvert "$nb" --to python --stdout | \
    perl -pe 's/def nb_/def py_/' \
    > "$py"
  done
done
